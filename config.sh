#!/bin/bash

if [[ $# -gt 0 ]]
then
  echo "configuring controller for cloudlab execution"
  printf "#!/bin/bash\n\n" > tests/ssh_vals.sh
  printf "user=$1\n" >> tests/ssh_vals.sh
  echo "/opt/hp4plus/hp4plus/hp4.json" > centralcontroller/hp4_json_path
  if [[ $# -gt 1 ]]
  then
    printf "nodes=( $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} )\n" >> tests/ssh_vals.sh
  fi
else
  echo "configuring controller for local (e.g., mininet) execution"
  rm -f centralcontroller/hp4_json_path
fi
